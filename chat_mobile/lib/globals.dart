library chat_mobile.globals;

import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:chat_models/chat_models.dart';


Future<SharedPreferences> flutterStorage =  SharedPreferences.getInstance();


class DataStore {

	static setVal(String k, String v ) async {

		SharedPreferences store = await flutterStorage;
		await store.setString(k, v);
	}

	static Future<bool> setToken(String v ) async {

		SharedPreferences store = await flutterStorage;
		return store.setString('token', v);
	}

	static Future<String> getToken() async {

		SharedPreferences store = await flutterStorage;
		return store.get('token') ;
	}

	static Future<bool> setUser(User v ) async {

		SharedPreferences store = await flutterStorage;
		return store.setString('user', jsonEncode(v.json));
	}

	static Future<User> getUser() async {

		SharedPreferences store = await flutterStorage;
		String userText = await store.get('user')  ?? null;
		if(userText != null) {
      return User.fromJson(jsonDecode(userText));
		}

		return null;
	}

	static Future<String> getVal(String k) async {
		SharedPreferences store = await flutterStorage;
		return await store.get(k);
	}

	static Future<bool> resetAuth() async {
		SharedPreferences store = await flutterStorage;
		await store.remove('token');
		await store.remove('user');
		authToken = null;
		currentUser = null;
		return true;
	}
}


const String host = '192.168.1.124';//'127.0.0.1';
const String webSocketAddress = 'ws://$host:3333/ws';
const String chatApiAddress = 'http://$host:3333';


String authToken;
User currentUser;

Future<bool> update() async {
  authToken = await DataStore.getToken();
	currentUser = await DataStore.getUser();
	return true;
}

