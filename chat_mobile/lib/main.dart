import 'dart:convert';

import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_list_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_received.dart';
import 'package:chat_mobile/blocs/message/message_bloc.dart';
import 'package:chat_mobile/blocs/message/message_list_bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:web_socket_channel/io.dart';

import 'blocs/auth/auth_user_bloc.dart';
import 'blocs/user/user_list_bloc.dart';

import 'globals.dart' as globals;
import './screens/login.dart';
import 'screens/navigation_screen.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // globals.DataStore.resetAuth();
  await globals.update();
  print('LOAD MAIN ${globals.currentUser}');
  runApp(SimpleChatApp());
}

class SimpleChatApp extends StatefulWidget {
  @override
  _SimpleChatAppState createState() => _SimpleChatAppState();
}

class _SimpleChatAppState extends State<SimpleChatApp> {


  @override
  Widget build(BuildContext context) {

    return MultiBlocProvider(
      providers: [
        BlocProvider<ChatBloc>(
          create: (context) => ChatBloc(),
        ),
        BlocProvider<ChatListBloc>(
          create: (context) => ChatListBloc(),
        ),
        BlocProvider<AuthUserBloc>(
          create: (context) => AuthUserBloc(),
        ),
        BlocProvider<UserListBloc>(
          create: (context) => UserListBloc()
        ),
        BlocProvider<MessageBloc>(
          create: (context) => MessageBloc()
        ),
        BlocProvider<MessageListBloc>(
          create: (context) => MessageListBloc()
        ),
        BlocProvider<ChatReceivedBloc>(
          create: (context) => ChatReceivedBloc()
        ),
      ],
      child:  MaterialApp(
        title: 'Simple Chat',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BlocBuilder<AuthUserBloc, AuthUserStates>(
          builder: (context, state) {

            if(state is AuthUserStateLogout) {
              return LoginPage();
            }


            if(globals.authToken != null) {

              IOWebSocketChannel channel = IOWebSocketChannel.connect('${globals.webSocketAddress}');

              return StreamBuilder(
                stream: channel.stream,
                builder: (context, snapshot) {

                  if(snapshot.hasData){

                    Message message = Message.fromJson(jsonDecode(snapshot.data));

                    print('RECERVED ${jsonDecode(snapshot.data)}');

                    BlocProvider.of<MessageBloc>(context)
                      ..add(MessageEventReceived(message));

                  }

                  return  NavigationScreen();

                },


              );

              // return BlocBuilder<ChatListBloc, ChatListStates>(
              //   builder: (context, state) {
              //
              //
              //   });



              // return NavigationScreen();

            }


            return LoginPage();
          })
      )
    );
  }
}


////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
