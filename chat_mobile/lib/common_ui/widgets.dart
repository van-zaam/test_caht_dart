import 'package:chat_mobile/blocs/auth/auth_user_bloc.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../globals.dart' as globals;



class LoadingView extends StatelessWidget {

	final String infoText;
	final GestureTapCallback updateDate;

	LoadingView({this.infoText, this.updateDate});

	@override
	Widget build(BuildContext context) {
		return Center(
			child: SizedBox(
				height: 200.0,
				width: 100.0,
				child: Column(
					children: [
						Text('${infoText ?? ''}'),
						Stack(
							children: [
								SizedBox(
									child: CircularProgressIndicator(
										strokeWidth: 10,
									),
									height: 100.0,
									width: 100.0,
								),
								infoText != null ? SizedBox(
									child: Center(
										child: FlatButton(
											shape: CircleBorder(side: BorderSide.none),
											child: Icon(Icons.update_sharp, size: 50,),
											onPressed: updateDate,
										),
									),
									height: 100.0,
									width: 100.0,
								) : SizedBox()
							],
						)
					],
				),
			)

		);
	}
}



class ShowUpdateIcon extends  StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
			child: CircularProgressIndicator(
				strokeWidth: 5,
				backgroundColor: Colors.white,
			),
			height: 50.0,
			width: 50.0,
		);
  }

}


class LogoutButton extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return IconButton(
			icon: Icon(Icons.exit_to_app),
			tooltip: 'Logout',
			onPressed: () {
				BlocProvider.of<AuthUserBloc>(context).add(AuthUserEventLogout());
			});
	}
}

class NotificationButton extends StatelessWidget {
	final Widget page;
	NotificationButton(this.page);
	@override
	Widget build(BuildContext context) {
		return IconButton(
			icon: Icon(Icons.add_alert_sharp),
			tooltip: 'Logout',
			onPressed: () {
				Navigator.of(context, rootNavigator: true)
					.push(MaterialPageRoute(builder:  (context) => page));
			});
	}
}



class SearchButton extends StatelessWidget {
	final GestureTapCallback onTap;
	SearchButton({this.onTap});
	@override
	Widget build(BuildContext context) {
		return IconButton(
			icon: Icon(Icons.search_outlined),
			tooltip: 'Search',
			onPressed: () {
				onTap();
			});
	}
}


class CreateChatButton extends StatelessWidget {
	final GestureTapCallback onTap;
	CreateChatButton({this.onTap});
	@override
	Widget build(BuildContext context) {
		return IconButton(
			icon: Icon(Icons.my_library_add ),
			tooltip: 'Logout',
			onPressed: () {
				onTap();
			});
	}
}

class MessageButton extends StatelessWidget {
	final GestureTapCallback onTap;
	final bool isAdded;
	final Color color;
	MessageButton({this.onTap, this.color = Colors.blue, this.isAdded = false});
	@override
	Widget build(BuildContext context) {
		return IconButton(
			icon: isAdded
				? Icon(Icons.message_rounded)
				: Icon(Icons.add_box),
			tooltip: 'Message',
			color: isAdded ? color : Colors.blue,
			onPressed: () {
				onTap();
			});
	}
}

///////////////////////
///////////////////////
///////ChatCard////////
///////////////////////
///////////////////////

class ChatCard extends StatelessWidget {


	final String name;
	final String title;
	final Color avaColor;
	final bool enabled;
	final bool selected;
	final GestureTapCallback onTap;
	final GestureTapCallback addChat;
	final bool  isAddedChat;
	final bool isCreateChat;
	final bool isGroup;
	final bool isChatCard;

	ChatCard({
		this.name,
		this.title,
		this.avaColor,
		this.enabled = true,
		this.selected = false,
		this.onTap,
		this.addChat,
		this.isAddedChat = false,
		this.isCreateChat = false,
		this.isGroup = false,
		this.isChatCard = false,
	});

	@override
	Widget build(BuildContext context) {

		List<String> listName = name.split(" ");
		String firstName = listName.first[0].toUpperCase();
		String secondName = listName.last[0].toUpperCase() ;
		String avaName = '$firstName$secondName';

    return  ListTile(
				enabled: enabled,
				onTap: () {
					onTap();
        },

				title: Container(
					height: 60,
					padding: EdgeInsets.zero,
					child: Row(
						children: [

							Expanded(
								flex: 1,
								child: CircleAvatar(
									backgroundColor: isGroup ? Colors.green: null,
									foregroundColor: isGroup ? Colors.white : avaColor ,
									radius: 30,
									child: isChatCard
										? isGroup
										? Icon(Icons.reduce_capacity_sharp)
										: Icon(Icons.account_circle_outlined)
										: Text('$avaName'),
								)
							),
							Expanded(
								flex: 3,
								child: Container(
									margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
									child: Text(title ?? name),
								)
							),
							selected ?
							Expanded(
								flex: 1,
								child: MessageButton(
									isAdded: isAddedChat,
									onTap: () {
										addChat();
									})
							):
							Expanded(
								flex: 1,
								child: Text('')
							),

							selected ?
							Expanded(
								flex: 1,
								child: Icon(Icons.chevron_right_sharp, size: 30, color: Colors.blue.shade400,)
							) :
							Expanded(
								flex: 1,
								child: Text('')
							),

						],
					),
				),
				hoverColor: Colors.grey.shade200,
				// selectedTileColor: Colors.grey.shade500,
				selected: selected,
				// tileColor:  null,
			);

	}
}


///////////////////////
///////////////////////
///////END ChatCard////////
///////////////////////
///////////////////////

///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
/////////// SEARCH FIELD //////////
/////////////////////////////////
////////////////////////////////

class FieldSearch extends StatelessWidget {
	final GestureTapCallback onPress;
	final TextEditingController controller;
	FieldSearch({this.onPress, this.controller});
  @override
  Widget build(BuildContext context) {
    return Row(
			children: [
				Expanded(
					flex: 5,
					child: Container(
						height: 40,
						decoration: BoxDecoration(
							color: Colors.white,
							borderRadius:  BorderRadius.all(Radius.circular(10))

						),
						padding: EdgeInsets.all(0),
						margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
						child: TextField(
							controller: controller,
							autofocus: true,
							decoration: InputDecoration(
								border: OutlineInputBorder(
									borderRadius: BorderRadius.all(Radius.circular(10)),
									borderSide: BorderSide(
										color: Colors.blue
									)
								),
								fillColor: Colors.black
								// labelText: 'Search',
							),
							style: TextStyle(
								height: .8,
							)
						),
					)
				),
				Expanded(
					flex: 0,
					child: Container(
						margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
						width: 80,
						child: FlatButton(
							textColor: Colors.white,
							child: Text('Cancel'),
							shape: RoundedRectangleBorder(
								borderRadius: BorderRadius.circular(10.0),
								// side: BorderSide(color: Colors.red)
							),
							onPressed: onPress,
						),
					)
				),

			],
		);

		// 	Container(
		// 	height: 60,
		// 	decoration: BoxDecoration(
		// 		color: Colors.blue
		// 	),
		// 	margin: EdgeInsets.zero,
		// 	padding: EdgeInsets.all(0),
		// 	child:
		//
		// );
  }
}


///////////////////////////////////
///////////////////////////////////
/////////// END  SEARCH FIELD //////////
/////////////////////////////////
////////////////////////////////

///////////////////////////////////
///////////////////////////////////
//// BUTTON TO SEARCH FIELD //////
/////////////////////////////////
////////////////////////////////

class ButtonToSearch extends StatelessWidget {
	final GestureTapCallback onPress;
	ButtonToSearch({this.onPress});
	@override
  Widget build(BuildContext context) {
    return Container(
			height: 60,
			decoration: BoxDecoration(
				color: Colors.blue

			),
			padding: EdgeInsets.all(10),
			child: RaisedButton(
				color: Colors.white,
				shape: RoundedRectangleBorder(
					borderRadius: BorderRadius.circular(10.0),
					// side: BorderSide(color: Colors.red)
				),
				child: Center(
					child: Stack(
						children: [
							Positioned(
								left: 0,
								top: 10,
								child: Icon(Icons.search)
							),
							Positioned(
								top: 12,
								left: 30,
								child: Text('search')
							)
						],
					)
				),
				onPressed: () {
					onPress();
				},
			),
		);
  }
}


///////////////////////////////////
///////////////////////////////////
////END  BUTTON TO SEARCH FIELD //////
/////////////////////////////////
////////////////////////////////

