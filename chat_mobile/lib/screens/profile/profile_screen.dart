
import 'package:chat_mobile/blocs/auth/auth_user_bloc.dart';
import 'package:chat_mobile/blocs/user/user_list_bloc.dart';
import 'package:chat_mobile/common_ui/widgets.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import '../../validate.dart';
import '../../globals.dart' as globals;

class ProfileScreen extends StatefulWidget {

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _phoneController = MaskedTextController(mask: '+7(000)000-00-00');

  final TextStyle textStyleTitle = TextStyle(
    fontSize: 20,
    fontWeight:
    FontWeight.bold
  );
  final TextStyle textStyleValue = TextStyle(
    fontSize: 20,
    fontStyle: FontStyle.italic,
    fontWeight: FontWeight.bold
  );

  final _formKey = GlobalKey<FormState>();


  User user = globals.currentUser;

  Widget divider = Divider(
    color: Colors.grey.shade700,
    height: 0,
    thickness: 2,
    indent: 0,
    endIndent: 0,
  );
  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        actions: <Widget>[LogoutButton()],
        automaticallyImplyLeading: false,
      ),
      body: BlocBuilder<UserListBloc, UserListStates>(

        builder: (context, state) {
          String name = user?.name ?? 'No name';
          String email = user?.email ?? 'No email';
          String phone = user?.phone ?? '';
          String password = user?.password ?? '';

          _emailController.text = email;
          _nameController.text = name;
          _passwordController.text = password;
          _phoneController.text = phone;

          // final snackBar = SnackBar(content: Text('UPDATE SAVE OK'));
          SnackBar createSnackBar(String message) => SnackBar(content: Text('$message'));


          return BlocListener<AuthUserBloc, AuthUserStates>(
            listener: (context, state) {

              if(state is AuthUserStateUpdate) {
                Scaffold.of(context).showSnackBar(SnackBar(content: Text('UPDATE SAVE OK')));
                globals.currentUser = state.user;
              }
              if(state is AuthUserStateError) {
                Scaffold.of(context).showSnackBar(SnackBar(content: Text('${state.errorMessage}')));
              }
            },
            child: RefreshIndicator(
              onRefresh:  () {
                return  _handleRefresh();
              },
              child: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      child: CircleAvatar(
                        radius: 80,
                        child: Text('$name'),
                      ),
                    ),
                    divider,
                    ListTile(
                      title: TextFormField(
                        controller: _emailController,
                        // initialValue: 'Jeefgf',
                        validator: validateEmail,
                        decoration: InputDecoration(
                          labelText: 'Email'
                        ),
                      ),
                    ),

                    ListTile(
                      title: TextFormField(
                        controller: _nameController,
                        decoration: InputDecoration(
                          labelText: 'Name'
                        ),
                      ),
                    ),
                    ListTile(
                      title: TextFormField(
                        validator: (v) {
                          if(v.length < 16) {
                            return 'Wrong number length!';
                          }
                          return null;
                        },
                        controller: _phoneController,
                        decoration: InputDecoration(
                          labelText: 'phone'
                        ),
                      ),
                    ),
                    ListTile(
                      title: TextFormField(
                        validator: (v) {
                          if(v.length < 4) {
                            return 'Wrong password length!';
                          }
                          return null;
                        },
                        controller: _passwordController,
                        decoration: InputDecoration(
                          labelText: 'Password'
                        ),
                      ),
                    ),
                    ListTile(
                      title: RaisedButton(
                        color: Colors.blue,
                        child: Text('Save', style: TextStyle(color: Colors.white)),
                        onPressed: () {

                          user.password = _passwordController.text;
                          user.phone = _phoneController.text;
                          user.email = _emailController.text;
                          user.name = _nameController.text;
                          if(this._formKey.currentState.validate()) {
                            BlocProvider.of<AuthUserBloc>(context)
                              ..add(AuthUserEventUpdate(user));
                            //////// SAVE
                          }
                        },
                      ),
                    )
                    // divider
                  ],
                ),

              )

            )
          );

        },
      ),
		);
  }

  Future<Null> _handleRefresh() async {
    return null;
  }
}
