
import 'package:chat_mobile/blocs/message/message_bloc.dart';
import 'package:chat_mobile/blocs/message/message_list_bloc.dart';
import 'package:chat_models/chat_models.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

// import 'package:chat_mobile/common_ui/widgets.dart'
import '../../globals.dart' as globals;

class ChatScreen extends StatefulWidget {
  ChatScreen({Key key, @required this.chat})
      : super(key: key);

  final Chat chat;
  final formatter = DateFormat('HH:mm');

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  String _title;
  var _messages = <Message>[];
  final _sendMessageTextController = TextEditingController();
  ScrollController scrollController = ScrollController();
  double scrollPosition = 0;
  bool _keyboardVisible = false;


  @override
  void initState() {
    super.initState();
    _title = widget.chat.members
        .where((user) => user.id != globals.currentUser.id)
        .map((user) => user.name)
        .join(", ");

    BlocProvider.of<MessageListBloc>(context)
      ..add(MessageListEventGet(widget.chat.id));
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body:
      GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child:  Container(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
        child: BlocListener<MessageBloc, MessageStates> (
          listener: (context, state) {

            if(state is MessageStateCreate) {
              //////////
            }
            if(state is MessageStateReceived)  {
              if(widget.chat.id  ==  state.message.chat) {
                setState(() {
                  _messages.add(state.message);
                });
              }
            }

          },
          child: BlocListener<MessageListBloc, MessageListStates> (
            listener: (context, state) {
              // print('LISTENER MESSAGES state $state');
              if(state is MessageListState) {
                setState(() {
                  _messages = state.messages;
                });
              }
            },
            child: BlocBuilder<MessageListBloc, MessageListStates>(
              builder: (context, state) {

                if(_keyboardVisible) {
                  // scrollController.jumpTo(scrollController.position.maxScrollExtent + 50);
                }

                List messagesRevers = _messages.reversed.toList().reversed.toList();
                return Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                        reverse: true,
                        controller: scrollController,
                        itemCount: messagesRevers.length,
                        itemBuilder: (BuildContext context, int index) {

                          return _buildListTile(messagesRevers [messagesRevers.length - index -1]);
                        },
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            // height: 60,
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: 'Your message',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  borderSide: BorderSide(
                                    color: Colors.blue
                                  )
                                ),
                                fillColor: Colors.black
                                // labelText: 'Search',
                              ),
                              // autofocus: true,
                              maxLines: 2,
                              keyboardType: TextInputType.multiline,
                              controller: _sendMessageTextController,
                            ),
                          )

                        ),
                        IconButton(
                          icon: const Icon(Icons.send),
                          onPressed: () {
                            if (_sendMessageTextController.text.isNotEmpty) {
                              send(_sendMessageTextController.text);
                              _sendMessageTextController.clear();
                            }
                          },
                        )
                      ],
                    ),
                  ],
                );

              },
            ),
          )
        )
      ),
      )
    );
  }

  // void _onAfterBuild(BuildContext context){
  //   scrollController.jumpTo(scrollController.position.maxScrollExtent + 50);
  // }

  Widget _buildListTile(Message message) {
    var isMyMessage = message.author.id == globals.currentUser.id;
    var messageTime = widget.formatter.format(message.createdAt);
    return _Bubble(
      message: message.text,
      isMe: isMyMessage,
      time: messageTime,
    );
  }

  send(String message) async {
    final newMessage = Message(
        chat: widget.chat.id,
        author: globals.currentUser,
        text: message,
        createdAt: DateTime.now()
    );
    BlocProvider.of<MessageBloc>(context)
      ..add(MessageEventLoading())
      ..add(MessageEventCreate(newMessage));
    setState(() {
      // _messages.add(newMessage);
      scrollController.jumpTo(scrollController.position.minScrollExtent);
    });
  }
}

class _Bubble extends StatelessWidget {
  _Bubble({this.message, this.time, this.isMe});

  final String message, time;
  final isMe;

  @override
  Widget build(BuildContext context) {
    final bg = isMe ? Colors.white : Colors.greenAccent.shade100;
    final align = isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end;
    final radius = isMe
        ? BorderRadius.only(
            topRight: Radius.circular(5.0),
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(5.0),
          )
        : BorderRadius.only(
            topLeft: Radius.circular(5.0),
            bottomLeft: Radius.circular(5.0),
            bottomRight: Radius.circular(10.0),
          );
    return Column(
      crossAxisAlignment: align,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(3.0),
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  blurRadius: .5,
                  spreadRadius: 1.0,
                  color: Colors.black.withOpacity(.12))
            ],
            color: bg,
            borderRadius: radius,
          ),
          child: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 48.0),
                child: Text(message),
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Row(
                  children: <Widget>[
                    Text(time,
                        style: TextStyle(
                          color: Colors.black38,
                          fontSize: 10.0,
                        )),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
