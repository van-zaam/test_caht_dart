import 'dart:async';
import 'dart:collection';

import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_list_bloc.dart';
import 'package:chat_mobile/blocs/user/user_list_bloc.dart';
import 'package:chat_mobile/common_ui/widgets.dart';
import 'package:chat_mobile/screens/chats/create_group_chat.dart';
import 'package:chat_mobile/screens/chats/chat_content.dart';
import 'package:chat_models/chat_models.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../globals.dart' as globals;


import '../../common_ui/widgets.dart';

class ChatListScreen extends StatefulWidget {
	@override
	_ChatListScreenState createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {

	bool searchOpen = false;
	bool showUpdateIcon = false;
	ChatListState chatListState = ChatListState([]);
	TextEditingController searchFieldController  = TextEditingController ();

	@override
	void initState() {
		super.initState();

		BlocProvider.of<ChatListBloc>(context)
			..add(ChatListEventLoading())
			..add(ChatListEventGet());

	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				elevation: 0.0,
				titleSpacing: 0.0,
				title: Text('Chats'),
				leading: Builder(
					builder: (BuildContext context) {
						return IconButton(
							icon: const Icon(Icons.my_library_add ),
							onPressed: () {
								Scaffold.of(context).openDrawer();
							},
							tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
						);
					},
				),
			),
			drawer: Drawer(
				elevation: 3,
				child: CreateChatScreen(title: 'Create group chat'),
			),
			body: BlocListener<ChatBloc, ChatStates>(
				listener: (context, state) {
					if(state is ChatStateCreate) {
						setState(() {
							chatListState.chats.add(state.chat);
						});
					}
				},
				child: BlocListener<ChatListBloc, ChatListStates>(
					listener:(context, state) {
						if(state is ChatListState) {
							setState(() {
								chatListState  = state;
							});
						}
					},
					child: BlocBuilder<ChatListBloc, ChatListStates>(builder: (context, state) {

						bool isExistChats = state is ChatListState && state.chats.length != 0;
						List<Chat> chats = chatListState.chats.reversed.toList();

						return RefreshIndicator(
							onRefresh: () async {
								BlocProvider.of<ChatListBloc>(context)
									..add(ChatListEventGet());
								return null;
							},
							child: isExistChats ?
							ListView.separated(
								physics: const AlwaysScrollableScrollPhysics(),
								separatorBuilder: (context, index) {
									return Divider(
										color: Colors.grey.shade100,
										height: 0,
										thickness: 2,
										indent: 60,
										endIndent: 0,
									);
								},
								itemCount: chats.reversed.length,
								itemBuilder: (context, index) {
									var  chat = chats[index];
									bool isGroup = chat.isGroup ?? false;
									var name = isGroup ? chats[index].title : chats[index].members.firstWhere(
											(e) => e?.id != globals.currentUser?.id).name;


									return ChatCard(
										isChatCard: true,
										isGroup: chat.isGroup ?? false,
										name: chat.isGroup ?? false ?  '${chat.title}' : "$name",
										onTap: () {
											Navigator.of(context, rootNavigator: true)
												.push(MaterialPageRoute(
												builder:  (context) => ChatScreen(chat: chat) ));
										},

									);
								}
							):
							ListView(
								children: [
									Container(
										margin: EdgeInsets.fromLTRB(0, 200, 0, 0),
										child: Center(
											child: Text('Oops, there is no chat yet'),
										)
									)
								],
							)
							,
						);
					}),
				)

			)

		);

	}

}




