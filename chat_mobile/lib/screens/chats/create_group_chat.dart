import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_list_bloc.dart';
import 'package:chat_mobile/blocs/user/user_list_bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../globals.dart' as globals;

class CreateChatScreen extends StatefulWidget {
  CreateChatScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _CreateChatScreenState createState() => _CreateChatScreenState();
}

class _CreateChatScreenState extends State<CreateChatScreen> {
  var _checkableUsers = <_CheckableUser>[];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _titleChatController  = TextEditingController ();
  List<User> usersWasChat  = [];


  void _loadCheckableUsers (List<Chat> chats) {
    List<_CheckableUser> users = [];
    for(Chat chat in chats){
      if(chat.members.length == 2) {
        User user = chat.members.firstWhere((u) => u.id != globals.currentUser.id, orElse: null);
        users.add(_CheckableUser(user: user));
      }
    }

    setState(() {
      _checkableUsers = users;
    });
  }
  @override
  void initState() {
    super.initState();

    ///// Берем данные из уже зашгруженного списка что бы не дел ать лишний звпрос к BD
    // var usersData = BlocProvider.of<UserListBloc>(context).state.props.first;
    var chatsData = BlocProvider.of<ChatListBloc>(context).state.props.first;
    if(chatsData is List<Chat>) {
      _loadCheckableUsers(chatsData);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: BlocListener<ChatListBloc, ChatListStates>(
        listener: (context, state) {
          if(state is ChatListState) {
            _loadCheckableUsers(state.chats);
          }
        },
        child: BlocBuilder<ChatBloc, ChatStates>(
          builder: (context, state) {
            return RefreshIndicator(
              onRefresh:  () async {
                BlocProvider.of<ChatListBloc>(context)
                  ..add(ChatListEventGet());
                return null;
              },
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: _checkableUsers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _buildListTile(_checkableUsers[index]);
                        },
                      ),
                    ),
                    Column(
                      children: [

                        Container(
                          height: 100,
                          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: TextFormField(
                            validator: (v) {
                              if(v.length < 3) {
                                return 'Title must be more 3 symbols';
                              } else {
                                return null;
                              }
                            },
                            controller: _titleChatController,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              hintText: 'Title group chat',
                              labelText: 'Title'
                            ),
                          ),
                        ),
                        Container(
                          // margin: EdgeInsets.fromLTRB(0, 0, 0, 40),
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 60),
                          child: RaisedButton(
                            child: Text("Create"),
                            onPressed: () {
                              createChat();
                            }),
                          
                        )

                      ],
                    )

                  ],
                )
              ),
              );
            },
          )
        )
    );

  }

  ListTile _buildListTile(_CheckableUser checkableUser) {
    return ListTile(
      title: Text(checkableUser.user.name ?? "no name"),
      trailing: Checkbox(
          value: checkableUser.isChecked,
          onChanged: (bool value) {
            setState(() {
              checkableUser.isChecked = value;
            });
          }),
    );
  }


  void createChat() async {
    var _checkedCounterparts = _checkableUsers
        .where((checkableUser) => checkableUser.isChecked == true)
        .map((checkableUser) => checkableUser.user)
        .toList();

    // print('=====$_checkedCounterparts');
    if (this._formKey.currentState.validate()) {
      if(_checkedCounterparts.isEmpty  || _checkedCounterparts.length < 2) {
        // BlocProvider.of<ChatBloc>(context)
        //   ..add(ChatEventLoading());
        print('ERROR MORE ');
      } else {
        BlocProvider.of<ChatBloc>(context)
          // ..add(ChatEventLoading())
          ..add(ChatEventCreate(
            _checkedCounterparts..add(globals.currentUser),
            title: _titleChatController.text,
            isGroup: true
          ));
        Navigator.of(context).pop();
      }
    }
  }
}

class _CheckableUser {
  final User user;
  bool isChecked;

  _CheckableUser({
    this.user,
    this.isChecked = false,
  });
}
