import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/blocs/auth/auth_user_bloc.dart';

import 'package:chat_mobile/common_ui/widgets.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../validate.dart';

import '../api_client.dart';
import '../globals.dart' as globals;

class LoginPage extends StatefulWidget {
	LoginPage({Key key}) : super(key: key);

	@override
	_LoginPageState createState() => _LoginPageState();
}

class _LoginData {
	String login = '';
	String password = '';
}

class _LoginPageState extends State<LoginPage> {
	final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
	_LoginData _loginData = _LoginData();
	final TextEditingController _loginController = TextEditingController();
	final TextEditingController _passwordController = TextEditingController();

	String _validateLogin(String value) {
		return validateEmail(value);
	}

	String _validatePassword(String value) {
		if (value.length < 2) {
			// check password rules here
			return 'The Password must be at least 2 characters.';
		}
		return null;
	}

	@override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

	@override
	Widget build(BuildContext context) {


		return Scaffold(body: Builder(
			builder: (BuildContext scaffoldContext) {


				return BlocListener<AuthUserBloc, AuthUserStates> (
					listener: (context, state) {
						if(state is AuthUserStateError) {
							final snackBar = SnackBar(
								content: Text('${state.errorMessage}')
							);
							Scaffold.of(context).showSnackBar(snackBar);
						};
					},
					child: BlocBuilder<AuthUserBloc, AuthUserStates>(
						builder: (context, state) {
							// print('STATE BUILDER LOGIN IN AUTH $state');
							return Stack(
								children: [
									SizedBox(
										child: state is AuthUserStateLoading ? LoadingView() : Text(''),
									),
									Container(
										padding: new EdgeInsets.all(20.0),
										child: Center(
											child: Form(
												key: this._formKey,
												child: Column(
													mainAxisAlignment: MainAxisAlignment.center,
													children: <Widget>[
														TextFormField(
															controller: _loginController,

															validator: this._validateLogin,
															onSaved: (String value) {
																this._loginData.login = value.trim();
															},
															decoration: InputDecoration(
																hintText: 'Email', labelText: 'Enter your Email'),
														),
														TextFormField(
															controller: _passwordController,
															obscureText: true,
															// Use secure text for passwords.
															validator: this._validatePassword,
															onSaved: (String value) {
																this._loginData.password = value;
															},
															decoration: new InputDecoration(
																hintText: 'Password', labelText: 'Enter your password'),
														),
														Container(
															margin: EdgeInsets.only(top: 20),
															child: Row(
																mainAxisAlignment: MainAxisAlignment.spaceEvenly,
																children: <Widget>[
																	RaisedButton(
																		child: Text("Login"),
																		onPressed: () {

																			_login(scaffoldContext);

																		}),
																	FlatButton(
																		child: Text("Sign up"),
																		onPressed: () {
																			_signUp(scaffoldContext);
																		},
																	),
																],
															),
														),

													],
												),
											),
										),
									)
								],
							);
						}
					),
				);
			},
		));
	}

	_signUp(BuildContext context) {
		if (this._formKey.currentState.validate()) {
			_formKey.currentState.save();
			_showDialog(_loginData.login).then((resultValue) {
				if (resultValue != null && resultValue is bool && resultValue) {
					UsersClient usersClient = UsersClient(MobileApiClient());
					usersClient
						.create(
						User(email: _loginData.login, password: _loginData.password, name: _loginData.login))
						.then((createdUser) {
						_clearUi();
						final snackBar =
						SnackBar(content: Text('User \'${createdUser.email}\' created'));
						Scaffold.of(context).showSnackBar(snackBar);
					}).catchError((signUpError) {
						final snackBar = SnackBar(
							content: Text('Sign up failed: ${signUpError.message}'));
						Scaffold.of(context).showSnackBar(snackBar);
						print('Sign up failed');
						print(signUpError);
					});
				}
			});
		}
	}

	_login(BuildContext context) async {
		if(this._formKey.currentState.validate()) {
			print('${_loginController.text.trim()}');
      BlocProvider.of<AuthUserBloc>(context)
				..add(AuthUserEventLoading())
				..add(AuthUserEventLogin(
					login: _loginController.text.trim(),
					password: _passwordController.text
				));
		}
	}


	Future<bool> _showDialog(String username) {
		return showDialog(
			context: context,
			builder: (BuildContext context) {
				return AlertDialog(
					content: new Text("Do you want to create user '$username' ?"),
					actions: <Widget>[
						new FlatButton(
							child: new Text("Cancel"),
							onPressed: () {
								Navigator.of(context).pop(false);
							},
						),
						new FlatButton(
							child: new Text("Ok"),
							onPressed: () {
								Navigator.of(context).pop(true);
							},
						),
					],
				);
			},
		);
	}

	void _clearUi() {
		_loginController.clear();
		_passwordController.clear();
	}
}
