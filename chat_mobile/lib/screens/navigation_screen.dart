import 'dart:collection';

import 'package:chat_mobile/blocs/chat/chat_list_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_received.dart';
import 'package:chat_mobile/blocs/message/message_bloc.dart';
import 'package:chat_mobile/blocs/message/message_list_bloc.dart';
import 'package:chat_mobile/screens/chats/chat_content.dart';
import 'package:chat_mobile/screens/profile/profile_screen.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'chats/chats_list_screen.dart';
import 'users/users_list_screen.dart';
import 'package:chat_mobile/globals.dart' as globals;


class NavigationScreen extends StatefulWidget {
	@override
	_NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {

	int _currentIndex = 0;

	List<Widget> _listPage = [
		UsersListScreen(),
		ChatListScreen(),
		ProfileScreen(),
	];

	List<Message> setMessage = [];


  @override
  Widget build(BuildContext context) {

		return new Scaffold(
			body: BlocListener<MessageBloc, MessageStates> (

				listener: (context, stateMessage) {
					if(stateMessage is MessageStateReceived)  {

						if(stateMessage.message.author.id != globals.currentUser.id) {
							setState(() {
								setMessage.add(stateMessage.message);
							});

							BlocProvider.of<ChatReceivedBloc>(context)
								..add(ChatReceivedEventGet());

						}
					}

				},
				child: BlocListener<ChatReceivedBloc, ChatReceivedStates>(
					listener: (context, state) {
						// print('START ??????  === == !!!!!!! $setMessage');
						if(state is ChatReceivedStateList) {
							Message msg = setMessage.first;

							setMessage.remove(msg);

							var chat = state.chats.firstWhere((chat) => chat.id == msg.chat, orElse: () => null);

							if(chat != null) {
								final snackBar = SnackBar(
									content: Text('${msg.author.name}'),
									duration: Duration(seconds: 5),

									action: SnackBarAction(
										label: 'Open',
										onPressed: () {
											Navigator.of(context, rootNavigator: true)
												.push(MaterialPageRoute(
												builder:  (context) => ChatScreen(chat: chat)));
										},
									),
								);

								Scaffold.of(context).showSnackBar(snackBar);
							}
						}
					},
					child: IndexedStack( // IndexedStack позволяет сохронять состояния виджетов при переключении табов
						index: _currentIndex,
						children: _listPage,
					),
				)
			),

			bottomNavigationBar: BottomNavigationBar(
				currentIndex: _currentIndex,
				onTap: onTabTapped ,
				items: [
					BottomNavigationBarItem(
						icon: Icon(Icons.home),
						label: "Users",
					),
					BottomNavigationBarItem(
						icon: Icon(Icons.mail),
						label: "Chats",
					),
					BottomNavigationBarItem(
						icon: Icon(Icons.account_box_sharp),
						label: "Profile",
					),
				],
			),

		);


  }

	void onTabTapped(int index) {
		setState(() {
			_currentIndex = index;
		});
	}
}
