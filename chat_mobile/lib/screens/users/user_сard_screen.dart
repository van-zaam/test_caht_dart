// import 'package:flutter/cupertino.dart';


import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';




class UserCardScreen extends StatefulWidget {
	final User user;
	UserCardScreen({this.user});
  @override
  _UserCardScreenState createState() => _UserCardScreenState();
}

class _UserCardScreenState extends State<UserCardScreen> {

	final TextStyle textStyleTitle = TextStyle(
		fontSize: 20,
		fontWeight:
		FontWeight.bold
	);
	final TextStyle textStyleValue = TextStyle(
		fontSize: 20,
		fontStyle: FontStyle.italic,
		fontWeight: FontWeight.bold
	);

  @override
  Widget build(BuildContext context) {
		String name = widget?.user?.name ?? 'No name';
		String email = widget?.user?.email ?? 'No email';
		String phone = widget?.user?.phone ?? 'No phone';

    return Scaffold(
			appBar: AppBar(
				title: Text('${widget?.user?.name ?? 'No name'}'),
			),
			body: ListView (

				children: [
					Container(
						padding: EdgeInsets.all(10),
						child: CircleAvatar(
							radius: 80,
							child: Text('$name'),
						),
					),

					Container(
						padding: EdgeInsets.all(10),
						child: Table(
							columnWidths: {1: FractionColumnWidth(0.75)},
							defaultColumnWidth: FlexColumnWidth(2.0),

							children: [
								TableRow(
									children: [
										TableCell(
											child: Text('Name: ', style: textStyleTitle),
										),
										TableCell(
											child: Text('$name', style: textStyleValue),
										),
									],
								),

								TableRow(
									children: [
										TableCell(
											child: Text('Phone', style: textStyleTitle),
										),
										TableCell(
											child: Text('$phone', style: textStyleValue),
										)
									]
								),
								TableRow(
									children: [
										TableCell(

											child: Text('Email:', style: textStyleTitle),
										),
										TableCell(
											child: Text('$email', style: textStyleValue),
										)
									]
								),
							],
						),
					)
				],
			)
		);

  }
}
