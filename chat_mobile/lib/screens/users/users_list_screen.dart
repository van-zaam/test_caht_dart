import 'dart:async';
import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/blocs/user/user_list_bloc.dart';
import 'package:chat_mobile/common_ui/widgets.dart';
import 'package:chat_mobile/screens/chats/chat_content.dart';
import 'package:chat_mobile/screens/nitifications/notification_screen.dart';

import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


import '../../common_ui/widgets.dart';
import '../../globals.dart' as globals;
import 'user_сard_screen.dart';

class UsersListScreen extends StatefulWidget {
	@override
	_UsersListScreenState createState() => _UsersListScreenState();
}

class _UsersListScreenState extends State<UsersListScreen> {
	ScrollController _controller;
	UserId _itemIdSelected;
	bool isOpenSnackBar = false;
	List<User> addedUserChat = [];
	UserListState userStateList = UserListState(users: [], myChats: []);
	bool searchOpen = false;
	bool showUpdateIcon = false;
	TextEditingController searchFieldController  = TextEditingController ();

	@override
	void initState() {
		super.initState();

    BlocProvider.of<UserListBloc>(context)..add(UserListEventGet());

		_controller = ScrollController();
	}

	int _getIndexExistingChat ({List<Chat> myChats, User currentUser, User user}) {
		/// Получаем все негрупповые чаты и находим существует ли чат с пользователем
		/// если нет, то получаем -1 если есть, то индекс чата в общем списке чатов
		/// пользователя
		List<Chat> chatsWithOneUser = myChats.where((chat) {
			return chat.members.length == 2;
		}).toList();

		if(chatsWithOneUser.isNotEmpty) {
			// print('111===111=1=1=1====${chatsWithOneUser[0]?.members}');
		}


    int indexExistingChat = chatsWithOneUser
			.indexWhere(
				(e) => e.members.first.id  == user?.id &&
				e.members.last?.id == currentUser?.id ||
				e.members.last?.id  == user?.id &&
					e.members.first?.id == currentUser?.id
		);

		return indexExistingChat;
	}

	bool _checkIsAddedChat({List<Chat> myChats, User currentUser, User user}) {
		bool isSelected = user.id == _itemIdSelected;

		if(isSelected ) {
			int index = _getIndexExistingChat(
				myChats: myChats,
				user: user,
				currentUser: currentUser
			);

      if(index >= 0) return true;
		}
		return false;
	}
	
	@override
	Widget build(BuildContext context) {

    return Scaffold(
			appBar: AppBar(
				elevation: 0.0,
				titleSpacing: 0.0,
				title: Text(' ${globals.currentUser.name}'),
				actions: !searchOpen ?  <Widget>[NotificationButton(NotificationScreen())] : null,
				automaticallyImplyLeading: false,
			),
			body: BlocListener<ChatBloc, ChatStates>(
				listener: (context, state) {
					if(state is ChatStateCreate) {
						setState(() {
							///Добавляем в состояние userStateList новый чат !!!
							///  Не делаем лишний запрос на сервер
							userStateList.myChats.add(state.chat);
						});
						///// Если чат не гшрупповой то переходим к чату!
						// if(!state.chat.isGroup) {
							Navigator.of(context, rootNavigator: true)
								.push(MaterialPageRoute(
								builder:  (context) => ChatScreen(chat: state.chat)));
					// 	}
					}
				},
				child: BlocBuilder<UserListBloc, UserListStates>(builder: (context, state) {
					if(state is UserListState) {
						userStateList = state;
					}
					UserId id = globals.currentUser?.id;
					User currentUser = globals.currentUser;

					List<Chat> chatsWithOneUser = userStateList.myChats.where((chat) {
						return chat.members.length == 2;
					}).toList();
					return RefreshIndicator(
						onRefresh:  () async {
							BlocProvider.of<UserListBloc>(context)
								..add(UserListEventGet());
							return null;
						},
						child: ListView.separated(
							separatorBuilder: (context, index) {
								return Divider(
									color: Colors.grey.shade100,
									height: 0,
									thickness: 2,
									indent: 60,
									endIndent: 0,
								);
							},
							physics: const AlwaysScrollableScrollPhysics(),
							controller: _controller,
							itemCount: userStateList.users.length,
							itemBuilder: (context, index) {
								User user = userStateList.users[index];;

								return ChatCard(
									title: id == user.id ?  "I'm ${user.email ?? ' '}"
										:'${user.name ?? ' '}  ${user.email ?? ' '}',
									name: user.name ?? ' ',
									enabled: id != user.id,
									avaColor: id == user.id ? Colors.black :  Colors.white,
									selected: user.id == _itemIdSelected,
									onTap: () {
										if(_itemIdSelected != user.id) {
											setState(() {
												_itemIdSelected = user.id;
											});
										} else {
											Navigator.of(context, rootNavigator: true)
												.push(MaterialPageRoute(builder:
												(context) => UserCardScreen(user: user)));
										}
									},
									isAddedChat: _checkIsAddedChat(
										user: user,
										currentUser: currentUser,
										myChats: userStateList.myChats
									),
									addChat: () {
										int indexExistingChat = _getIndexExistingChat(
											myChats: userStateList.myChats,
											currentUser: currentUser,
											user: user
										);


										if(indexExistingChat >= 0) {
											print('is CHAT : ${chatsWithOneUser[indexExistingChat].id}');
											Navigator.of(context, rootNavigator: true)
												.push(MaterialPageRoute(
												builder:  (context) =>
													ChatScreen(chat: chatsWithOneUser[indexExistingChat])));
										} else {

											BlocProvider.of<ChatBloc>(context)
												..add(ChatEventCreate([user, currentUser]));
										}
									},
								);

							},
						),
					);

				}),
			)

		);
	}

}
