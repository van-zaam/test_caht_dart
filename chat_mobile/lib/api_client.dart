import 'package:chat_api_client/chat_api_client.dart';

import 'globals.dart' as globals;

class MobileApiClient extends ApiClient {
  MobileApiClient()
      : super(Uri.parse(globals.chatApiAddress),
            onBeforeRequest: (ApiRequest request) {
          if (globals.authToken != null)
            return request.change(
                headers: {}
                  ..addAll(request.headers)
                  ..addAll({'authorization': globals.authToken}));
          return request;
        }, onAfterResponse: (ApiResponse response) {
          if (response.headers.containsKey('authorization'))
            // TODO: добавить проверку на исключение при записи в storage
            globals.DataStore.setToken(response.headers['authorization'])
              .then((value) {
                if(value)  globals.authToken = response.headers['authorization'];
            });

          return response;
        });
}
