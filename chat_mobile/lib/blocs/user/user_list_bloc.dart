import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../api_client.dart';


///// STATES ///////
class UserListStates extends Equatable {
  @override
  List<Object> get props => [];
}

class UserListState extends UserListStates {
	final List<User> users;
	final List<Chat> myChats;
	UserListState({this.users, this.myChats});
	@override
	List<Object> get props => [users, myChats];
}

class UserListStateLoading extends  UserListStates{
	UserListStateLoading() : super();
}

class UserListStateError extends UserListStates {
	final String errorMessage;
	UserListStateError(this.errorMessage);
	List<Object> get props => [errorMessage];
}

//////END STATES ///////

///////// EVENTS ////////

class UserListEvents extends Equatable {
	@override
	List<Object> get props => [];
}

class UserListEventGet extends UserListEvents {
	UserListEventGet() : super();
}

class UserListEventLoading extends UserListEvents {
	UserListEventLoading() : super();
}

//////// END EVENTS ////////


////////// BLOC ////////////

class UserListBloc extends Bloc<UserListEvents, UserListStates> {
	UsersClient usersClient;
	ChatsClient chatsClient;

  UserListBloc() : super(UserListStateLoading()) {
		usersClient = UsersClient(MobileApiClient());
		chatsClient = ChatsClient(MobileApiClient());
	}

  @override
  Stream<UserListStates> mapEventToState(UserListEvents event) async* {

		if(event is UserListEventLoading) {
			yield UserListStateLoading();
		}

		if(event is UserListEventGet) {
			try {
				List<User> foundUsers = await usersClient.read({});
				List<Chat> foundMyChats = await chatsClient.read({});

				// print('LOAD USERS');
				yield UserListState(users: foundUsers, myChats: foundMyChats);

			} on Exception catch (e) {
				print('Failed to get list of chats');
				yield UserListStateError('Error connect  or access is denied. Update data, please!');
			}
		}
  }


}
//////////END BLOC ////////////

