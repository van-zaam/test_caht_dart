import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../api_client.dart';
import '../../globals.dart' as globals;




///////// STATES ////////////
///////// STATES ////////////
///////// STATES ////////////
///////// STATES ////////////
class AuthUserStates extends Equatable {
  @override
  List<Object> get props =>  [];
}

class AuthUserStateLogin extends AuthUserStates{
	final User user;
	AuthUserStateLogin(this.user);
	@override
	List<Object> get props =>  [user];
}

class AuthUserStateError extends AuthUserStates{
	final String errorMessage;
	AuthUserStateError(this.errorMessage);
	List<Object> get props => [errorMessage, DateTime.now()];
}
class AuthUserStateLoading extends AuthUserStates {}
class AuthUserStateLogout extends AuthUserStates {}

class AuthUserStateUpdate extends AuthUserStates {
	final User user;
  AuthUserStateUpdate(this.user);
	@override
	List<Object> get props =>  [user];
}

///////// END STATES ////////////
///////// END STATES ////////////
///////// END STATES ////////////
///////// END STATES ////////////


////////// EVENTS //////////
////////// EVENTS //////////
////////// EVENTS //////////
////////// EVENTS //////////
class AuthUserEvents extends Equatable {
	@override
	List<Object> get props => [];
}
class AuthUserEventLogin extends AuthUserEvents {
	final String login;
	final String password;
	AuthUserEventLogin({this.login, this.password});
	@override
	List<Object> get props =>  [];
}
class AuthUserEventUpdate extends AuthUserEvents {
	final User user;
	AuthUserEventUpdate(this.user);
	@override
	List<Object> get props =>  [user];

}


class AuthUserEventLoading extends AuthUserEvents {}
class AuthUserEventLogout extends AuthUserEvents {}

////////// END EVENTS //////////
////////// END EVENTS //////////
////////// END EVENTS //////////
////////// END EVENTS //////////


////////////// BLOC //////////
////////////// BLOC //////////
////////////// BLOC //////////
////////////// BLOC //////////

class AuthUserBloc extends Bloc<AuthUserEvents, AuthUserStates> {

	UsersClient usersClient;

  AuthUserBloc() : super(null) {
		usersClient = UsersClient(MobileApiClient());
	}

  @override
  Stream<AuthUserStates> mapEventToState(AuthUserEvents event) async* {

  	if(event is AuthUserEventLoading) {
  		yield AuthUserStateLoading();
		}

		if(event is AuthUserEventLogout) {
			globals.DataStore.resetAuth();
			yield AuthUserStateLogout();
		}

		if(event is AuthUserEventLogin) {
			String login = event.login.trim();
			String password = event.password;
      User user;
			try {
				user = await usersClient.login(login, password);

			} catch (err) {
				// print('EROOR LOGIN = $err |');
				yield AuthUserStateError('Login failed');
			}

			if(user is User) {

        if(await globals.DataStore.setUser(user)) globals.update();
				yield AuthUserStateLogin(user);
			} else {
        yield AuthUserStateError('Login failed');
			}
		}

		if(event is AuthUserEventUpdate) {
			User user = event.user;

			try {

				user = await usersClient.update(user);
        yield AuthUserStateUpdate(user);
			} catch (err) {
				yield AuthUserStateError('Update failed');
			}
		}
  }

}
////////////// END BLOC //////////
////////////// END BLOC //////////
////////////// END BLOC //////////
////////////// END BLOC //////////



