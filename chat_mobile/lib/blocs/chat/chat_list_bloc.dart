import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


///////// STATES ////////////

class ChatListStates extends Equatable {
	@override
	List<Object> get props => [];
}

class ChatListStateLoading extends ChatListStates{
	ChatListStateLoading() : super();
}

class ChatListStateError extends ChatListStates{
	final String errorMessage;
	ChatListStateError(this.errorMessage);
	List<Object> get props => [errorMessage];
}
class ChatListState extends ChatListStates {
	final List<Chat> chats;
	ChatListState(this.chats);
	List<Object> get props => [chats];
}

class ChatsListStateReceived extends ChatListStates {
	final List<Chat> chats;
	ChatsListStateReceived(this.chats);
	List<Object> get props => [chats];
}
///////// END STATES //////////


////////// EVENTS ////////////////

class ChatListEvents extends Equatable {
	@override
	List<Object> get props => [];
}

class ChatListEventLoading extends ChatListEvents {
	ChatListEventLoading() : super();
}

class ChatListEventGet extends ChatListEvents {}


class ChatsListEventReceived extends ChatListEvents {}
////////// END EVENTS /////////////


////////////// BLOC ////////////////
class ChatListBloc extends Bloc<ChatListEvents, ChatListStates> {

	UsersClient usersClient;
	ChatsClient chatsClient;

	ChatListBloc() : super(ChatListStateLoading()) {
		usersClient = UsersClient(MobileApiClient());
		chatsClient = ChatsClient(MobileApiClient());
	}


	@override
	Stream<ChatListStates> mapEventToState(ChatListEvents event) async* {

		if(event is ChatListEventLoading) {
			yield ChatListStateLoading();
		}

		if(event is ChatListEventGet) {
			print('LOAD CHAT LIST');
      try {

				List<Chat> found = await chatsClient.read({});
				// print('~~~~~~~~GET LIST CHAT ChatEventGetChats $found ');
				yield ChatListState(found);
			} on Exception catch (e) {
				print('Failed to get list of chats');
				yield ChatListStateError('No authorization');
			}
		}

		if(event is ChatsListEventReceived) {

			try {
				List<Chat> found = await chatsClient.read({});
				// print('~~~~~~~~GET LIST CHAT ChatEventGetChats $found ');
				yield ChatsListStateReceived(found);
			} on Exception catch (e) {
				print('Failed to get list of chats');
				yield ChatListStateError('No authorization');
			}
		}
	}
}