import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


///////// STATES ////////////
class ChatStates extends Equatable {
	@override
	List<Object> get props => [];
}

class ChatStateLoading extends ChatStates{
	ChatStateLoading() : super();
}

class ChatStateError extends ChatStates{
	final String errorMessage;
	ChatStateError(this.errorMessage);
	List<Object> get props => [errorMessage];
}

class ChatStateCreate extends ChatStates {
	final Chat chat;
	ChatStateCreate(this.chat);
	@override
	List<Object> get props => [chat];
}
class ChatStateRemove extends ChatStates {
	final Chat chat;
	ChatStateRemove(this.chat);
	@override
	List<Object> get props => [chat];
}


///////// END STATES //////////

////////// EVENTS ////////////////
class ChatEvents extends Equatable {
	@override
	List<Object> get props => [];
}

class ChatEventLoading extends ChatEvents {
	ChatEventLoading() : super();
}

class ChatEventRemoveChat extends ChatEvents {
	final Chat chat;
	ChatEventRemoveChat(this.chat);
	List<Object> get props => [chat];
}

class ChatEventCreate extends ChatEvents {
	final List<User> members;
	final String title;
	final bool isGroup;
	ChatEventCreate(this.members, {this.title, this.isGroup});
	List<Object> get props => [members, title, isGroup];
}
////////// END EVENTS /////////////

class ChatBloc extends Bloc<ChatEvents, ChatStates> {

	UsersClient usersClient;
	ChatsClient chatsClient;

	ChatBloc() : super(ChatStateLoading()) {
		usersClient = UsersClient(MobileApiClient());
		chatsClient = ChatsClient(MobileApiClient());
	}

	@override
	Stream<ChatStates> mapEventToState(ChatEvents event) async* {

		if(event is ChatEventLoading) {
			yield ChatStateLoading();
		}


		if(event is ChatEventCreate) {
			// print('CREATE CHAT ${event.members}');
      try {
				Chat createdChat = await chatsClient.create(
					Chat(members: event.members, title: event.title, isGroup: event.isGroup),
				);
				// print('CREATE CHAT ${createdChat}');

        yield ChatStateCreate(createdChat);
			} on Exception catch (e) {
				print('Failed to get list of chats');
				yield ChatStateError('$e');
			}

		}

		if(event is ChatEventRemoveChat) {
			try {
				Chat removeChat = await chatsClient.delete(event.chat);

				yield ChatStateRemove(removeChat);
			} on Exception catch (e) {
				print('Failed to get list of chats');
				yield ChatStateError('$e');
			}
		}
	}
}