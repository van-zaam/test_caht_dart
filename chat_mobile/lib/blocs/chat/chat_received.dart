import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


///////// STATES ////////////

class ChatReceivedStates extends Equatable {
	@override
	List<Object> get props => [];
}


class ChatReceivedStateError extends ChatReceivedStates{
	final String errorMessage;
  ChatReceivedStateError(this.errorMessage);
	List<Object> get props => [errorMessage];
}
class ChatReceivedStateList extends ChatReceivedStates {
	final List<Chat> chats;
	ChatReceivedStateList(this.chats);
	List<Object> get props => [chats];
}


///////// END STATES //////////


////////// EVENTS ////////////////

class ChatReceivedEvents extends Equatable {
	@override
	List<Object> get props => [];
}

class ChatReceivedEventGet extends ChatReceivedEvents {}
////////// END EVENTS /////////////


////////////// BLOC ////////////////
class ChatReceivedBloc extends Bloc<ChatReceivedEvents, ChatReceivedStates> {

	ChatsClient chatsClient;

	ChatReceivedBloc() : super(null) {
		chatsClient = ChatsClient(MobileApiClient());
	}


	@override
	Stream<ChatReceivedStates> mapEventToState(ChatReceivedEvents event) async* {


		if(event is ChatReceivedEventGet) {

			try {
				List<Chat> found = await chatsClient.read({});
				yield ChatReceivedStateList(found);
			} on Exception catch (e) {
				print('Failed to get list of chats');
				yield ChatReceivedStateError('No authorization');
			}
		}
	}
}