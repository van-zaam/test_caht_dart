import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/api_client.dart';
import 'package:chat_mobile/blocs/message/message_list_bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


////////////////////////////////
////////////////////////////////
//////////// STATES //////////////
////////////////////////////////
////////////////////////////////

class MessageStates extends Equatable {
  @override
  List<Object> get props => [];

}

class MessageStateLoading extends MessageStates {
	MessageStateLoading() : super();
}

class MessageStateError extends MessageStates {
	final String messageError;
	MessageStateError(this.messageError);
	@override
	List<Object> get props => [messageError];
}

class MessageStateGetList extends MessageStates {

}

class MessageStateCreate extends MessageStates {
	final Message message;

  MessageStateCreate(this.message);
  List<Object> get props => [message];

}

class MessageStateReceived extends MessageStates {
	final Message message;

  MessageStateReceived(this.message);

	List<Object> get props => [message];
}
////////////////////////////////
////////////////////////////////
///////// END STATES ///////////
////////////////////////////////
////////////////////////////////

////////////////////////////////
////////////////////////////////
//////////// EVENTS ////////////
////////////////////////////////
////////////////////////////////


class MessageEvents extends Equatable {
	@override
	List<Object> get props => [];
}

class MessageEventLoading extends MessageEvents{
	MessageEventLoading() : super();
}

class MessageEventError extends MessageListEvents {
	MessageEventError() : super();
}

class MessageEventCreate extends MessageEvents  {
	final Message message;
  MessageEventCreate(this.message);
	@override
	List<Object> get props => [message];
}


class MessageEventReceived extends MessageEvents {
	final Message message;

  MessageEventReceived(this.message);
	@override
	List<Object> get props => [message];
}
////////////////////////////////
////////////////////////////////
///////// END EVENTS////////////
////////////////////////////////
////////////////////////////////

////////////////////////////////
////////////////////////////////
//////////// BLOC //////////////
////////////////////////////////
////////////////////////////////

class MessageBloc extends Bloc<MessageEvents, MessageStates> {

  MessageBloc() : super(MessageStateLoading());


  @override
  Stream<MessageStates> mapEventToState(MessageEvents event) async* {

  	if(event is MessageEventCreate) {
  		Message newMessage = event.message;
			try {
				await MessagesClient(MobileApiClient()).create(newMessage);
				yield MessageStateCreate(newMessage);

			} on Exception catch (e) {
				print('Sending message failed');
				print(e);
				yield MessageStateError(e.toString());

			}
		}

  	if(event is MessageEventReceived) {
  		yield MessageStateReceived(event.message);
		}
  }

}


////////////////////////////////
////////////////////////////////
///////// END BLOC /////////////
////////////////////////////////
////////////////////////////////