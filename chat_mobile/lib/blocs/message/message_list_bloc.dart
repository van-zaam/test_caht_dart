import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


////////////////////////////////
////////////////////////////////
//////////// STATES //////////////
////////////////////////////////
////////////////////////////////

class MessageListStates extends Equatable {
  @override
  List<Object> get props => [];

}

class MessageListStateLoading extends MessageListStates {
	MessageListStateLoading() : super();
}

class MessageListStateError extends MessageListStates {
	final String messageError;
	MessageListStateError(this.messageError);
	@override
	List<Object> get props => [messageError];
}

class MessageListState extends MessageListStates {
	final List<Message>  messages;
	MessageListState(this.messages);
	@override
	List<Object> get props => [messages];
}


////////////////////////////////
////////////////////////////////
///////// END STATES ///////////
////////////////////////////////
////////////////////////////////

////////////////////////////////
////////////////////////////////
//////////// EVENTS ////////////
////////////////////////////////
////////////////////////////////


class MessageListEvents extends Equatable {
	@override
	List<Object> get props => [];
}

class MessageListEventLoading extends MessageListEvents {
	MessageListEventLoading() : super();
}

class MessageListEventError extends MessageListEvents {
	MessageListEventError() : super();
}

class MessageListEventGet extends MessageListEvents {
	final ChatId chatId;
	MessageListEventGet(this.chatId) : super();
	@override
	List<Object> get props => [chatId];
}
////////////////////////////////
////////////////////////////////
///////// END EVENTS////////////
////////////////////////////////
////////////////////////////////

////////////////////////////////
////////////////////////////////
//////////// BLOC //////////////
////////////////////////////////
////////////////////////////////

class MessageListBloc extends Bloc<MessageListEvents, MessageListStates> {

  MessageListBloc() : super(MessageListStateLoading());

  @override
  Stream<MessageListStates> mapEventToState(MessageListEvents event) async* {


  	if(event is MessageListEventLoading)  {
			yield MessageListStateLoading();
		}

  	if(event is MessageListEventGet) {
  		ChatId chatId = event.chatId;
			// print('START UPDATE LIST MessagesStateList chatId $chatId');

			try {
				List<Message> messages = await MessagesClient(MobileApiClient()).read(chatId);

				// print('UPDATE LIST MessagesStateList $messages');
        yield MessageListState(messages);

			} on Exception catch (e) {
				print('Failed to get list of messages');
				print(e);
				yield MessageListStateError(e.toString());
			}
		}
  }

}


////////////////////////////////
////////////////////////////////
///////// END BLOC /////////////
////////////////////////////////
////////////////////////////////